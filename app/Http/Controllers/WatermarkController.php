<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yasapurnama\DocumentWatermark\WatermarkFactory;
use Ajaxray\PHPWatermark\Watermark;


class WatermarkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function watermarkPdf(Request $request)
    {
        $params = $this->validate($request, [
            'file' => 'required|file',
            'watermark' => 'required',
            'selectable' => 'nullable',
        ]);

        $bytes = random_bytes(6);
        $uuid = bin2hex($bytes);
        $date = date('YmdHis');
        $temp = "temp";
        $output = "output";
        $fileName = "source-{$uuid}.pdf";
        $sourceFile = storage_path("{$temp}/{$fileName}");

        if (!file_exists(storage_path($temp))) mkdir(storage_path($temp), 0777, true);
        if (!file_exists(storage_path($output))) mkdir(storage_path($output), 0777, true);

        // save temp source pdf to local
        $request->file('file')->move(storage_path($temp), $fileName);
        $img = $this->pngGenerator($params['watermark'], $uuid);

        // watermarking custom label
        $outputFile = $this->watermark($sourceFile, $img, $uuid);

        // watermarking copy
        if (request('selectable')) {
            $this->setCopy($outputFile);
        } else {
            $watermark = new Watermark($outputFile);
            $watermark->setFontSize(100)->setRotate(40)->setOpacity(0.1)->withText('C O P Y');
        }

        // remove temp png
        unlink($img);
        // remove source
        unlink($sourceFile);
        $attachment_location = $outputFile;
        if (file_exists($attachment_location)) {
            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: application/pdf");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:" . filesize($attachment_location));
            header("Content-Disposition: attachment; filename={$uuid}.pdf");
            readfile($attachment_location);
            die();
        } else {
            die("Error: File not found.");
        }
    }

    function pngGenerator($string, $uuid = 'temp')
    {
        $pathTemp = storage_path("temp/{$uuid}.png");
        $font = 40;
        $im = @imagecreatetruecolor(strlen($string) * $font / 1.5, $font);
        imagesavealpha($im, true);
        imagealphablending($im, false);
        $white = imagecolorallocatealpha($im, 255, 255, 255, 127);
        imagefill($im, 0, 0, $white);
        $lime = imagecolorallocate($im, 0, 0, 0);
        imagestring($im, $font, 0, 0, $string, $lime);
        imagepng($im, $pathTemp);
        imagedestroy($im);
        return $pathTemp;
    }

    function watermark($source, $png, $uuid)
    {
        $output = storage_path("output/{$uuid}.pdf");
        WatermarkFactory::load($source)
            ->outputFile($output)
            ->setImage($png)
            ->sectionFooter()
            // ->alignRight()
            // ->alignLeft(100, 500)
            ->opacity(0.5)
            ->generate();
        return $output;
    }

    function setCopy($source)
    {
        $png = storage_path('copy.png');
        $uuid = 'tes';
        $output = $source; // storage_path("output/{$uuid}.pdf");
        WatermarkFactory::load($source)
            ->outputFile($output)
            ->setImage($png)
            // ->sectionFooter()
            // ->alignRight()
            ->alignLeft(0, 0)
            ->opacity(0.1)
            ->generate();
    }
}
