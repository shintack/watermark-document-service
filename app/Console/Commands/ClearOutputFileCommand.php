<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ClearOutputFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:output';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display an inspiring quote';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = storage_path('output');
        $files = array_values(array_diff(scandir($path), array('.', '..')));
        if (!empty($files)) {
            foreach ($files as $file) {
                $f = "{$path}/{$file}";
                try {
                    unlink($f);
                    $this->info("Deleted: {$f}");
                } catch (\Throwable $th) {
                    //throw $th;
                    $msg = $th->getMessage();
                    Log::info($msg);
                    $this->error($msg);
                }
            }
        }
    }
}
